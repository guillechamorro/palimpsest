#!/bin/bash
# License: GPL 
# Author: Guillermo Chamorro
# Description: Change parameter of original grub.cfg from every system installed

# ask for sudo
ask_for_sudo() {
  if [ "${EUID}" != 0 ]; then
    sudo "$0" "$@"
    exit $?
  fi
}

# virtual path of the palimpsest scripts
readonly PALIMPSEST_PATH="$( cd "$(dirname "$0")" ; pwd -P )"

#
# call to libraries
#
source "${PALIMPSEST_PATH}"/style
source "${PALIMPSEST_PATH}"/install_packages
source "${PALIMPSEST_PATH}"/scan_and_select_usb
source "${PALIMPSEST_PATH}"/scan_and_select_isos
source "${PALIMPSEST_PATH}"/create_usb
source "${PALIMPSEST_PATH}"/install_grub
source "${PALIMPSEST_PATH}"/delete_systems
source "${PALIMPSEST_PATH}"/uninstall_systems
source "${PALIMPSEST_PATH}"/check_integrity
source "${PALIMPSEST_PATH}"/check_disk_space
source "${PALIMPSEST_PATH}"/install_systems
source "${PALIMPSEST_PATH}"/install_windows
source "${PALIMPSEST_PATH}"/available_systems
source "${PALIMPSEST_PATH}"/check_for_internet_connection
source "${PALIMPSEST_PATH}"/create_directories_and_files
source "${PALIMPSEST_PATH}"/mount_actions
source "${PALIMPSEST_PATH}"/install_grub_configuration
source "${PALIMPSEST_PATH}"/install_hirens_boot

#
# global variables
#

# backtitle of dialogs
readonly BACKTITLE="PALIMPSEST - Multiboot USB creation"
# the isos selected when creating the USB
SELECTED_SYSTEM_ON_CREATE=

# the isos selected when deleting them
SELECTED_SYSTEM_ON_DELETE=

# the name without path of the isos
SELECTED_SYSTEM_NAME=

# the selected USB disk with the prefix /dev/
SELECTED_DISK_DEV=

# the created partition on the disk, sdX1
DISK_PARTITION=

# the directory where the DISK_PARTITION will be mounted
USB_MOUNT_DIR_BOOT_PARTITION=/tmp/mount_dir_boot_partition

# the directory where the DISK_PARTITION will be mounted
USB_MOUNT_DIR_LINUX_PARTITION=/tmp/mount_dir_linux_partition

# the directory where the DISK_PARTITION will be mounted
USB_MOUNT_DIR_WINDOWS_PARTITION=/tmp/mount_dir_windows_partition

# the directory where the DISK_PARTITION will be mounted
USB_GRUB_DIR="${USB_MOUNT_DIR_BOOT_PARTITION}"/boot/grub

# the main grub config file thet prompts the system added
MAIN_GRUB_CFG_FILE="${USB_MOUNT_DIR_BOOT_PARTITION}"/boot/grub/grub.cfg

# the i386 grub.cfg file
GRUB_PC_CFG_FILE="${USB_MOUNT_DIR_BOOT_PARTITION}"/grub/grub.cfg

# the directory where the linux ISOS will be mounted
ISOS_MOUNT_DIR=/tmp/isos_mount_dir

# directoy in the usb where the isos are stored
USB_STORE_ISOS_DIR="${USB_MOUNT_DIR_LINUX_PARTITION}"/isos/

# store the value to know if the action is create, install, or uninstall an ISO
CREATE_DELETE_OR_INSTALL_SYSTEM=0

# the file in the USB where the installed ISOS are registered
REGISTER_INSTALLED_SYSTEMS_FILE="${USB_MOUNT_DIR_BOOT_PARTITION}"/.installed_systems

# the main configuration file 
CONFIGURATION_PALIMPSEST_FILE="${USB_MOUNT_DIR_BOOT_PARTITION}"/.palimpsest

# the README file in the USB
README_FILE="${USB_MOUNT_DIR_BOOT_PARTITION}"/README.txt

# store the local directory of isos
SAVE_LOCAL_DIR_FILE=/tmp/save_local_dir
#
PARTITION_BOOT=1
#
PARTITION_LINUX_TWO=2
#
INSTALL_ONLY_LINUX=1
#
INSTALL_WINDOWS=0
#
INSTALL_ONLY_WINDOWS=0
#
INSTALL_WINDOWS_7_AND_10=0
#
WINDOWS_MOUNT_ISO_DIR=/tmp/mount_windows_7_iso
#
WINDOWS_MOUNT_PARTITION_DIR=/tmp/mount_windows_partition_one
#
PARTITION_WINDOWS_ONE=2
#
MEMDISK_FILE="${PALIMPSEST_PATH}"/extras/memdisk
#
MEMDISK_FILE_USB="${USB_MOUNT_DIR_BOOT_PARTITION}"/isos/memdisk

# the program exits
goodbye() {
  umount_usb_partitions
  echo
  echo -e "${LIGHT_YELLOW}Programa terminado"
  echo
  sleep 0.3
  exit
}

# once the program has finalized, ask what to do next
end() {
  dialog --backtitle "${BACKTITLE}" \
  --title "Finalizado" \
  --yesno "¿Desea realizar alguna otra acción?" 0 0
  local response=$?
  case "${response}" in
    0) welcome;;
    1) goodbye;;
    255) goodbye;;
  esac
}

check_if_exists_installation() {
  INSTALLED=0
  local format_ok=0
  local grub_ok=0
  if [ -f "${CONFIGURATION_PALIMPSEST_FILE}" ]; then
    while read -r line; do
      if [ "$line" == "grub_ok" ]; then
        grub_ok=1
      fi
      if [ "$line" == "format_ok" ]; then
        format_ok=1
      fi
    done < "${CONFIGURATION_PALIMPSEST_FILE}"
  fi
  if [ ! -f "${CONFIGURATION_PALIMPSEST_FILE}" ] || [ "${format_ok}" -eq 0 ]  || [ "${grub_ok}" -eq 0 ]; then
    dialog --backtitle "${BACKTITLE}" \
    --title "Instalación" \
    --yesno "No existe una instalación de PALIMPSEST. ¿Desea crear una ahora?" 0 0
    local response=$?
    case "${response}" in
      0) ;;
      1) end;;
      255) end;;
    esac
    INSTALLED=0
  else
    echo -e "${LIGHT_YELLOW}YA EXISTE UNA INSTALACIÓN DE PALIMPSEST!!${WHITE}"
    sleep 1
    INSTALLED=1
  fi
}

sanitize_iso_names() {
  ISO_NAME=$(echo ${ISO_NAME} | sed -e 's/ /_/g')
  ISO_NAME=$(echo ${ISO_NAME} | sed -e 's/'\''//g')
  ISO_NAME=$(echo ${ISO_NAME} | sed -e 's/\[/_/g')
  ISO_NAME=$(echo ${ISO_NAME} | sed -e 's/\]/_/g')
}

# unicode font and background image
extras() {
  local backgroud_image="${PALIMPSEST_PATH}"/extras/palimpsest.png
  local unicodepf2="${PALIMPSEST_PATH}"/extras/unicode.pf2
  if ! [ -f "${USB_MOUNT_DIR_BOOT_PARTITION}"/boot/grub/"${backgroud_image}" ]; then
    echo -e "${LIGHT_GREEN}Copiando ${WHITE}${backgroud_image}${LIGHT_GREEN} a ${WHITE}${USB_MOUNT_DIR_BOOT_PARTITION}/boot/grub/${LIGHT_GREEN}...${WHITE}"
    sleep 0.5
    cp "${backgroud_image}" "${USB_MOUNT_DIR_BOOT_PARTITION}"/boot/grub/
    echo -e "${LIGHT_GREEN}Copiando ${WHITE}${backgroud_image}${WHITE} a ${LIGHT_GREEN}${USB_MOUNT_DIR_BOOT_PARTITION}/boot/grub/${WHITE}"
    sleep 1
  else
    echo -e "${LIGHT_YELLOW}El archivo ${WHITE}${backgroud_image}${LIGHT_YELLOW} ya existe. {WHITE}"
    sleep 0.5
  fi
  if ! [ -f "${USB_MOUNT_DIR_BOOT_PARTITION}"/boot/grub/${unicodepf2} ]; then
    echo -e "${LIGHT_GREEN}Copiando ${WHITE}${backgroud_image}${LIGHT_GREEN} a ${WHITE}${USB_MOUNT_DIR_BOOT_PARTITION}/boot/grub/${WHITE}"
    sleep 0.5
    cp "${unicodepf2}" "${USB_MOUNT_DIR_BOOT_PARTITION}"/boot/grub/
    echo -e "${LIGHT_GREEN}Copiando ${WHITE}${unicodepf2}${WHITE} a ${LIGHT_GREEN}${USB_MOUNT_DIR_BOOT_PARTITION}/boot/grub/${LIGHT_GREEN}...${WHITE}"
    sleep 1
  else
    echo -e "${LIGHT_YELLOW}El archivo ${WHITE}${unicodepf2}${LIGHT_YELLOW} ya existe. {WHITE}"
    sleep 0.5
  fi
}

#
# the initial dialog to select main options of the program
#
welcome() {
  local welcome_options=(1 "Agregar un sistema"   2 "Desintalar un sistema" 3 "Crear un USB con Window\$ 7 o Window\$ 10" 4 "Crear un USB con Window\$ 7 y 10" 5 "Salir")
  choice=$(dialog --clear \
       --backtitle "${BACKTITLE}" \
       --title "${WELCOME_TITLE}" \
       --menu "Bienvenido. ¿Qué desea hacer?" \
       15 60 6 \
       "${welcome_options[@]}" \
       2>&1 > /dev/tty)
  case ${choice} in
    1) 
      CREATE_DELETE_OR_INSTALL_SYSTEM=1
      INSTALL_ONLY_LINUX=1
      mount_linux_partition
      main_scan_and_select_isos
      main_install_systems
      end
      ;;
    2) 
      CREATE_DELETE_OR_INSTALL_SYSTEM=3
      mount_linux_partition
      main_scan_and_select_isos
      main_uninstall_systems
      end
      ;;
    3) 
      CREATE_DELETE_OR_INSTALL_SYSTEM=1
      INSTALL_WINDOWS=1
      INSTALL_ONLY_WINDOWS=1
      #main_scan_and_select_isos
      main_windows_install
      ;;
    4) 
      INSTALL_WINDOWS=1
      INSTALL_WINDOWS_7_AND_10=1
      ;;
    5) goodbye;;
    "") goodbye;;
    255) goodbye;;
  esac
}

main() {
  ask_for_sudo
  main_install_packages
  main_scan_usb
  umount_usb_partitions
  mount_boot_partition
  check_if_exists_installation
  umount_usb_partitions
  if [[ "${INSTALLED}" = 0 ]]; then
    main_create_usb
    main_install_grub
    extras
    umount_usb_partitions
  fi
  welcome
}

main




